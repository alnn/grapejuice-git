# grapejuice-git
This is an unofficial installer for a [modified version of the Grapejuice project](https://gitlab.com/TestingPlant/grapejuice/-/tree/master/).
Note that this is not currently avaliable on the Arch User Repository.

# Installing
Check if you have the multilib repository enabled by running
```bash
if pacman -Sl multilib &> /dev/null; then
    echo "multilib is enabled"
else
    echo "multilib is not enabled"
fi
```

If you don't have the multilib repository enabled, you can run the below or manually edit `/etc/pacman.conf`
```bash
printf "\n\
# multilib is enabled for Wine\n\
\n\
[multilib]\n\
Include = /etc/pacman.d/mirrorlist\n" >> /etc/pacman.conf
```

Run
```bash
git clone --depth=1 https://gitlab.com/TestingPlant/grapejuice-git
makepkg -si
```

If you get an error similar to `python-psutil: /usr/lib/python3.8/site-packages/* exists in filesystem`, you have a package downloaded through pip instead of pacman. To solve this, you need to run `pip uninstall <package>` as root. With this example, run `pip uninstall psutil`.
If the error has `python-gobject`, remove `pygobject`. If it has `python-dbus`, remove `dbus-python`. With any other package, use the pattern used above.

You might find that the desktop icon entry doesn't appear the first time you launch the application. You can manually launch it by running
```bash
grapejuice gui
```

# Uninstalling
**Do not run 1x_uninstall.sh!** That uninstaller is not supported when installing with this MAKEPKG. Instead, you should run
```bash
pacman -R grapejuice
```

If you want to remove your wineprefix, you can run
```bash
rm -r "~/.local/share/grapejuice"
```

You might also want to disable the multilib repository. Make sure you don't have any packages installed from multilib such as `wine`, and then edit `/etc/pacman.conf` with a text editor. If you used the command in the installation part to enable it, you'll find the below text in the file
```
# multilib is enabled for Wine

[multilib]
Include = /etc/pacman.d/mirrorlist
```
You can delete those lines above to disable the multilib library.
